//#define PERIODIC_BOUNDARY_CONDITION

#include <iostream>
#include <fstream>
#include <sstream>
#include <cmath>
#include <ctime>
#include <algorithm>
#include "c2dvector.h"
#include "parameters.h"
#include "particle.h"
#include "cell.h"
#include "box.h"
#include "wall.h"

inline void timing_information(clock_t start_time, int i_step, int total_step)
{
		clock_t current_time = clock();
		int lapsed_time = (current_time - start_time) / (CLOCKS_PER_SEC);
		int remaining_time = (lapsed_time*(total_step - i_step)) / (i_step + 1);
		cout << "\r" << round(100.0*i_step / total_step) << "% lapsed time: " << lapsed_time << " s		remaining time: " << remaining_time << " s" << flush;
}


inline void equilibrium(Box* box, int equilibrium_step, int saving_period, ofstream& out_file, ofstream& traj_file)
{
	clock_t start_time = clock();
	cout << "equilibrium:" << endl;
//	box->Init();
	for (int i = 0; i < equilibrium_step; i++)
	{
		box->Multi_Step(10);
		timing_information(start_time,i,equilibrium_step);
//		if (i % saving_period == 0)
//		{
//			out_file << box;
//			box->Make_Traj(3, traj_file);
//		}
	}
	cout << "Finished" << endl;
}


inline void data_gathering(Box* box, int total_step, int saving_period, ofstream& out_file, ofstream& traj_file)
{
	clock_t start_time = clock();
	cout << "gathering data:" << endl;
	int saving_time = 0;

	for (int i = 0; i < total_step; i++)
	{
		box->Multi_Step(10);
		timing_information(start_time,i,total_step);

		if (i % saving_period == 0)
		{
			out_file << box;
//			box->Make_Traj(3, traj_file);

//			stringstream address;
//			address.str("");
//			address << "data-" << saving_time << ".dat";
//			ofstream velocity_field_file(address.str().c_str());
//			box->Save_Fields(velocity_field_file);
//			velocity_field_file.close();

			saving_time++;
		}
	}

	cout << "Finished" << endl;
}


int main(int argc, char *argv[])
{
	C2DVector::Init_Rand(time(NULL));
//	C2DVector::Init_Rand(2352);

	Box box;
	box.Init(atoi(argv[1]), sqrt(2*atof(argv[2])));


	cout << "bah bah" << endl; 
	cout << "intinger = " << atoi(argv[1]) << endl; 

	stringstream address;
	address.str("");
	address << box.info.str() << "-trajectory.xyz";
	ofstream traj_file(address.str().c_str());

	address.str("");
	address << box.info.str() << "-r-v.dat";
	ofstream out_file(address.str().c_str());

	equilibrium(&box, equilibrium_step, saving_period, out_file, traj_file);
	data_gathering(&box, total_step, saving_period, out_file, traj_file);
	traj_file.close();
	out_file.close();
}

