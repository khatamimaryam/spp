#include <GL/gl.h>
#include <GL/glu.h>
#include <GL/glut.h>
#include <opencv/cv.h>
#include <opencv/highgui.h>

#include <cstdlib>
#include <vector>
#include <iostream>
#include <sstream>
#include <fstream>
#include "shapes.h"
#include "c2dvector.h"

unsigned int window_width = 700;
unsigned int window_height = 700;

vector<Scene> scene;

cv::VideoWriter writer;

int t = 0;
bool save = false;
float box_dim = 0;

float Readfile(ifstream& input_file)									// finds size of the box(default=max of particles x in all frames)
{
	static Scene temp_scene;
	float L = 20.;														// but here size of my box is 33. 
	while (!input_file.eof())
	{
		input_file >> temp_scene;
		scene.push_back(temp_scene);
//	for (int i = 0; i < Scene::number_of_particles; i++)
//		if (temp_scene.particle[i].r.x > L)
//			L = temp_scene.particle[i].r.x;
	}
	input_file.close();
//	L = round(1000*L)/1000;
	return (L+2.);
}

void Init()												//
{
	// select eraser (backbgroud) color 
	glClearColor (1.0, 1.0, 1.0, 0.0);
	// single color mode to fill polygons and lines
	glShadeModel (GL_FLAT);
	Init_Circle();
	Particle::radius = 0.2;
//	Particle::tail = 0.2;
	Particle::color.red = 1.0;
	Particle::color.green = 0.2;
	Particle::color.blue = 0.2;
}

void Display()
{
	// lets erase! 
	glClear(GL_COLOR_BUFFER_BIT);
	//glPushMatrix();
//	glPopMatrix();

	// Show the barriers
	glColor3f(0.0, 0.0, 0.0);
	glLineWidth(10.);

	// Show Box 
	glBegin(GL_LINE_STRIP);
	glVertex3f(L, L, 0.);
	glVertex3f(L, -L, 0.);
	glVertex3f(-L, -L, 0.);
	glVertex3f(-L, L, 0.);
	glVertex3f(L, L, 0.);
	glEnd();


	glColor3f(1.0, 0.2, 0.2);
	// default line width for particles in motion
	glLineWidth(1.);

	scene[t].Draw();

	if (save)
	{
		static cv::Mat img(window_width,window_height,CV_8UC3);
		glPixelStorei(GL_PACK_ALIGNMENT, (img.step & 3) ? 1 : 4);
		glPixelStorei(GL_PACK_ROW_LENGTH, img.step/img.elemSize());
		glReadPixels(0, 0, img.cols, img.rows, GL_BGR, GL_UNSIGNED_BYTE, img.data);
//		cv::flip(img, flipped, 0);
		writer << img;
	}

	glutSwapBuffers();
}


void Next_Frame(void)
{
	t++;
	if (t < 0)
		t = 0;
	if (t >= scene.size())
		t = scene.size() - 1;
	glutPostRedisplay();
}

void SpecialInput(int key, int x, int y)
{
	switch(key)
	{
		case GLUT_KEY_PAGE_UP:
			t += 99;
		break;
		case GLUT_KEY_PAGE_DOWN:
			t -= 101;
		break;
		case GLUT_KEY_UP:
			t += 9;
		break;	
		case GLUT_KEY_DOWN:
			t -= 11;
		break;
		case GLUT_KEY_LEFT:
			t -= 2;
		break;
		case GLUT_KEY_RIGHT:
			t += 0;
		break;
		default:
		break;
	}
	Next_Frame();
}

void KeyboardInput(unsigned char key, int x, int y)
{
	static bool stop = true;
	if ((key == 32) || (key = 112))
		stop = !stop;
	if (stop)
		glutIdleFunc(NULL);
	else
		glutIdleFunc(Next_Frame);
}


void Reshape(int w, int h)
{
	window_width = w;
	window_height = h;

	glViewport (0, 0, (GLsizei) w, (GLsizei) h);
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	glOrtho(-box_dim, box_dim,  -box_dim, box_dim, -1.0, 1.0);
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
}


int main(int argc, char** argv)
{
	int glargc = 1;
	char** glargv;

	if (argc > 2)
		save = true;

	stringstream address("");
	address << argv[argc-1];
	ifstream data_file;
	data_file.open(address.str().c_str());
	box_dim = Readfile(data_file);								// 
	data_file.close();

	string name = address.str().c_str();
	string::size_type position_of_txt = name.find("-r-v.dat", 0);				// 
	name.erase(position_of_txt);
	address.str("");
	address << name << ".mpg";
	if (save)
		writer.open(address.str().c_str(), CV_FOURCC('P','I','M','1'), 20, cv::Size(window_width,window_height), true);

	glutInit(&glargc, glargv);
	glutInitDisplayMode (GLUT_DOUBLE | GLUT_RGB);
	glutInitWindowSize (window_width, window_height);
	glutInitWindowPosition (100, 100);
	glutCreateWindow ("Created By Hamid!");
	Init ();
	glutDisplayFunc(Display);
	glutReshapeFunc(Reshape);
	glutSpecialFunc(SpecialInput);
	glutKeyboardFunc(KeyboardInput);
	glutMainLoop();

	return 0;
}

