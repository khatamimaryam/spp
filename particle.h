#ifndef _PARTICLE_
#define _PARTICLE_

#include "c2dvector.h"
#include "parameters.h"
#include "wall.h"

class Particle{
public:
	C2DVector r,v,f;
	Index cell_index, field_cell_index;
//	Real torque; 
	Real theta;
	C2DVector average_v;
	int neighbor_size;
	static Real noise_amplitude;
	static Real v0;

	Particle();
	void Init();
	void Init(C2DVector position);
	void Init(C2DVector position, C2DVector velocity);
	void Move()
	{
		C2DVector e_hat; 
//		theta += (g*torque + gsl_ran_gaussian(C2DVector::gsl_r,noise_amplitude))*dt;
		theta = atan(average_v.y/average_v.x);
		if (average_v.x < 0)
			theta += PI;
		theta += gsl_ran_flat (C2DVector::gsl_r, -noise_amplitude, noise_amplitude);
		// drop theta in interval (0 < theta < 2*PI)
		theta -= 2*PI * (int (theta / (2*PI)));
		if (theta < 0)
			theta += 2*PI; 

		C2DVector old_v = v;
		e_hat.x = cos(theta);
		e_hat.y = sin(theta);
		v = e_hat * v0 + f;
		r += (old_v + v)*(half_dt);
//		r += v*dt;
		#ifdef PERIODIC_BOUNDARY_CONDITION
			r.Periodic_Transform();
		#endif
		Reset();
	}

	void Reset();

	void Interact(Particle* p)
	{
		C2DVector dr = r - p->r;
		#ifdef PERIODIC_BOUNDARY_CONDITION
			dr.Periodic_Transform();
		#endif
		Real d2 = dr.Square();
		Real d = sqrt(d2);

		Real torque_interaction;

		C2DVector interaction_force;

		if (d < 1.)		// r_f = 1
		{
			neighbor_size++;
			p->neighbor_size++;
//			torque_interaction = sin(p->theta - theta)/PI;
//			torque += torque_interaction;
//			p->torque -= torque_interaction;
			average_v += p->v;
			p->average_v += v;

//			interaction_force = dr * A_e * exp(- d / sigma_e ) / d2;
//			f += interaction_force;
//			p->f -= interaction_force;
		}

		if (d < 2*r_e)
		{
			interaction_force = dr * A_e * (2 * r_e - d) / d;
			f += interaction_force;
			p->f -= interaction_force;
		}
	}

	void Interact(Wall w)
	{
		C2DVector dr_1, dr_2, dr;
		Real d2_1, d2_2, d2;
		dr_1 = r - w.point_1;
		dr_2 = r - w.point_2;
		#ifdef PERIODIC_BOUNDARY_CONDITION
			dr_1.Periodic_Transform();
			dr_2.Periodic_Transform();
		#endif

		C2DVector interaction_force;
		if ((dr_1*w.direction < w.length) && (dr_1*w.direction > 0))
		{
			dr = dr_1 - (w.direction)*(dr_1*w.direction);
			d2 = dr.Square();
		}
		else
		{
			d2_1 = dr_1.Square();
			d2_2 = dr_2.Square();
			if (d2_2 < d2_1)
			{
				dr = dr_2;
				d2 = d2_2;
			}
			else
			{
				dr = dr_1;
				d2 = d2_1;
			}
		}

		Real d = sqrt(d2);

//		if (d < .7)
//		{
//			interaction_force = dr * A_e * exp(- d / sigma_e ) / d2;
//			f += interaction_force;
//		}

		if (d < (r_e + r_w))
		{
			interaction_force = dr * A_w * (r_e + r_w - d) / d;
			f += interaction_force;
		}
	}
};

Particle::Particle()
{
	Init();
}

void Particle::Init()
{
	r.Rand();
	v.Rand(1.0);
	theta = atan(v.y/v.x);
	if (v.x < 0)
		theta += PI;
	Reset();
}

void Particle::Init(C2DVector position)
{
	r = position;
	v.Rand(1.0);
	theta = atan(v.y/v.x);
	if (v.x < 0)
		theta += PI;
	Reset();
}

void Particle::Reset()
{
	neighbor_size = 1;
//	torque = 0;
	average_v = v;
	f.Null();
}

Real Particle::noise_amplitude = 0.35;
Real Particle::v0 = 2.;


#endif
