#ifndef _PARAMETERS_
#define _PARAMETERS_

using namespace std;

typedef double Real;

int seed = 1241;

const int divisor = 30;

const Real PI = 3.1415;

const int max_wall_num = 100;
const int max_N = 5000;

// Box
const Real L = 20;
const Real L2 = 2*L;
const int grid_size = 40;

// Time
const Real dt = 0.002;
const Real half_dt = dt/2;
const int equilibrium_step = 30000;
const int total_step = 10000;
const int saving_period = 10;

// Interactions
const Real A_e = 100.;
const Real r_e = 0.05; 
const Real A_w = 200.;
const Real r_w = 0.07; 
//const Real g = 1.; 
//const Real sigma_e = .7; 
//const Real sigma_w = .7; 

// Funnel Barriers 
static Real l_b = 4.9; 
static Real l_0 = 0.6; 
static Real l_s = 5.5; 
static Real alpha = 30. * PI/ 180.; //(in radian)

#endif
