#ifndef _WALL_
#define _WALL_

#include "c2dvector.h"
#include "parameters.h"
#include "particle.h"

class Wall{
public:
	C2DVector point_1,point_2;
	C2DVector direction, normal;
	Real length;
	Index cell_index, field_cell_index;

	Wall();
	void Init(C2DVector input_p_1, C2DVector intput_p_2);
};

Wall::Wall()
{
}

void Wall::Init(C2DVector input_p_1, C2DVector input_p_2)
{
	point_1 = input_p_1;
	point_2 = input_p_2;
	direction = point_2 - point_1;
	#ifdef PERIODIC_BOUNDARY_CONDITION
		direction.Periodic_Transform();
	#endif
	length = sqrt(direction.Square());
	direction = direction / length;
	normal.x = -direction.y;
	normal.y = direction.x;
}



#endif
