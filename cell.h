#ifndef _CELL_
#define _CELL_

#include "c2dvector.h"
#include "parameters.h"
#include <vector>

class Cell{
public:
	vector<Particle*> particle;
	C2DVector r;

	Cell();

//	void Init();
	void Delete();
	void Add(Particle* p);
	void Interact(Cell c);
	void Self_Interact();
};

Cell::Cell()
{
}
/*
void Cell::Init(Real x, Real y)
{
	r.x = x;
	r.y = y;
}
*/
void Cell::Delete()
{
	particle.clear();
}

void Cell::Add(Particle* p)
{
	particle.push_back(p);
}

void Cell::Interact(Cell c)
{
	for (int i = 0; i < particle.size(); i++)
	{
		for (int j = 0; j < c.particle.size(); j++)
			particle[i]->Interact(c.particle[j]);
	}
}

void Cell::Self_Interact()
{
	for (int i = 0; i < particle.size(); i++)
	{
		for (int j = i+1; j < particle.size(); j++)
			particle[i]->Interact(particle[j]);
	}
}


#endif
