#include "c2dvector.h"

const int circle_points_num = 100;

GLfloat unitcircle[2*circle_points_num];
GLuint indices[circle_points_num];

void Init_Circle()
{
	for (int i = 0; i < circle_points_num; i++)
	{
		unitcircle[2*i] = cos(2*i*PI/circle_points_num);	// x_i
		unitcircle[2*i+1] = sin(2*i*PI/circle_points_num);	// y_i
		indices[i] = i;						// i (point index)
	}
}

struct RGB{
	float red,green,blue;
};

class Particle{
public:
	C2DVector r,v;
	static float radius;
	static float tail;
	static RGB color;
	void Draw();
};

float Particle::radius;
float Particle::tail;
RGB Particle::color;

void Particle::Draw()
{
	// allow to put all points coordinates of a circle in an array
	// GL_VERTEX_ARRAY is a type of array that can save coordinates 
	glEnableClientState (GL_VERTEX_ARRAY);
	// put! =put all points of a circle in an array.  glVertexPointer(number of coordinates of each element, type, 0, name of array)
	glVertexPointer (2, GL_FLOAT, 0, unitcircle);

	// location of each partice is a transformation of default particle to an amount of its own (x,y)
	glTranslatef(r.x,r.y,0);
	// each particle was composition of a number of points on a unit circle. lets scale it to a circle of given radius. 
	glScalef(radius,radius,1);

	// obtain array all elements (mode, count, type, indices)
	glDrawElements(GL_POLYGON,circle_points_num,GL_UNSIGNED_INT,indices);

	// tail of each point
	float scale = 2;
	glBegin(GL_LINES);
		glVertex3f(0, 0, 0.0);	//?
		glVertex3f(-scale*v.x,-scale*v.y, 0);
	glEnd();
	
	glDisableClientState( GL_VERTEX_ARRAY );
	// before the viewing transformation can be specified, the current matrix is set to the identity matrix
	glLoadIdentity();
}

class Scene{
public:
	static float L;
	static int number_of_particles;
	static const int max_particle_num = 5000;
	Particle particle[max_particle_num];
	void Draw();
	friend std::istream& operator>>(std::istream& is, Scene& scene);
};

float Scene::L;
int Scene::number_of_particles;

void Scene::Draw()
{
	// glColor takes 3 parameters, which are, in order, the red, green, and blue components of the color.
	glColor3f(Particle::color.red, Particle::color.green, Particle::color.blue);
	for (int i = 0; i < number_of_particles; i++)
		particle[i].Draw();
}

std::istream& operator>>(std::istream& is, Scene& scene)
{
	is >> scene.number_of_particles;
	for (int i = 0; i < scene.number_of_particles; i++)
	{
		is >> scene.particle[i].r;
		is >> scene.particle[i].v;
	}
}


