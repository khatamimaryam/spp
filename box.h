#ifndef _BOX_
#define _BOX_

#include <iostream>
#include <sstream>
#include <fstream>
#include "c2dvector.h"
#include "parameters.h"
#include "particle.h"
#include "cell.h"
#include "field-cell.h"
#include "wall.h"


class Box{
public:
	int N, wall_num;
	Particle particle[max_N];
	Wall wall[100];
	Cell cell[divisor][divisor];
	Field_Cell field_cell[grid_size][grid_size];
	C2DVector v_field[grid_size][grid_size];
	C2DVector p[100];

	Real density;
	stringstream info;

	Box();
	void Init(int particle_num, Real noise_amplitude);
	void Update_Cells();
	void Interact();
	void Move();
	void One_Step();
	void Multi_Step(int steps);
	void Update_Field_Cells();
	void Compute_Fields();
	void Save_Fields(ofstream& data_file);

	void Make_Traj(Real scale, std::ofstream& data_file);

	friend std::ostream& operator<<(std::ostream& os, Box* box);
	friend std::istream& operator>>(std::istream& is, Box* box);
};

Box::Box()
{
	N = 0;
	density = 0;
}

void Box::Init(int particle_num, Real noise_amplitude)
{
	N = particle_num;
	density = (Real) N / (L2*L2);

	int Nx = (int) sqrt(N) + 1;
	C2DVector v_cm,r;
	for (int i = 0; i < N; i++)
	{
		particle[i].Init();
		v_cm += (particle[i].v / N);
	}
	for (int i = 0; i < N; i++)
		particle[i].v -= v_cm;

	Particle::noise_amplitude = noise_amplitude;

	Update_Cells();

	// Box Boundries (A Square)
	p[0].x = p[1].x = -L;
	p[2].x = p[3].x = L;
	p[0].y = p[3].y = -L;
	p[1].y = p[2].y = L;
	wall[0].Init(p[0], p[1]);
	wall[1].Init(p[1], p[2]);
	wall[2].Init(p[2], p[3]);
	wall[3].Init(p[3], p[0]);	
	// So all in all we have 28 walls as wall barriers which consists of 40 points
	wall_num = 4;

	for (int i = 0; i < grid_size; i++)
		for (int j = 0; j < grid_size; j++)
			field_cell[i][j].Init((Real) L*(2*i-grid_size + 0.5)/grid_size, (Real) L*(2*j-grid_size + 0.5)/grid_size);
	info.str("");
	info << "rho=" << density << "-noise=" << (Particle::noise_amplitude)*(Particle::noise_amplitude)/2;
}


void Box::Update_Cells()
{
	for (int x = 0; x < divisor; x++)
		for (int y = 0; y < divisor; y++)
			cell[x][y].Delete();

	#pragma omp parallel for
	for (int i = 0; i < N; i++)
	{
		int x,y;
		x = (int) (particle[i].r.x + L)*divisor / (2*L);
		y = (int) (particle[i].r.y + L)*divisor / (2*L);
		cell[x][y].Add(&particle[i]);
	}
}


void Box::Interact()
{
	for (int x = 0; x < divisor; x++)
		for (int y = 0; y < divisor; y++)
		{
			cell[x][y].Self_Interact();
			cell[x][y].Interact(cell[(x+1)%divisor][y]);
			cell[x][y].Interact(cell[x][(y+1)%divisor]);
			cell[x][y].Interact(cell[(x+1)%divisor][(y+1)%divisor]);
			cell[x][y].Interact(cell[(x+1)%divisor][(y-1+divisor)%divisor]);
		}

	for(int i=0 ; i<N; i++)
		for(int j= 0; j < wall_num; j++)
			particle[i].Interact(wall[j]);
}


void Box::Move()
{
	for (int i = 0; i < N; i++)
		particle[i].Move();
}

void Box::One_Step()
{
	Interact();
	Move();
}

void Box::Multi_Step(int steps)
{
	for (int i = 0; i < steps; i++)
	{
		Interact();
		Move();
	}
	Update_Cells();
}

void Box::Update_Field_Cells()
{
	for (int x = 0; x < grid_size; x++)
		for (int y = 0; y < grid_size; y++)
			field_cell[x][y].Delete();

	for (int i = 0; i < N; i++)
	{
		int x = (int) (particle[i].r.x + L)*grid_size / (2*L);
		int y = (int) (particle[i].r.y + L)*grid_size / (2*L);
		field_cell[x][y].Add(&particle[i]);
	}
}

void Box::Compute_Fields()
{
	Update_Field_Cells();
	for (int i = 0; i < grid_size; i++)
		for (int j = 0; j < grid_size; j++)
			field_cell[i][j].Compute_Fields();
	for (int i = 0; i < grid_size; i++)
		for (int j = 0; j < grid_size; j++)
			field_cell[i][j].curl = (grid_size/L)*(field_cell[(i+1)%grid_size][j].v.y - field_cell[i][j].v.y) - (grid_size/L)*(field_cell[i][(j+1)%grid_size].v.x - field_cell[i][j].v.x);
}

void Box::Save_Fields(ofstream& data_file)
{
	Compute_Fields();
	for (int i = 0; i < grid_size; i++)
	{
		for (int j = 0; j < grid_size; j++)
			data_file << field_cell[i][j].r << "\t" << field_cell[i][j].v << "\t" << field_cell[i][j].density << "\t"<< field_cell[i][j].phi << "\t" << field_cell[i][j].curl << "\t" << field_cell[i][j].omega << endl;
		data_file << endl;
	}
}

void Box::Make_Traj(Real scale, ofstream& data_file)
{
	data_file << N << endl;
	data_file << "something" << endl;
	for (int i = 0; i < N; i++)
		data_file << "H	" << particle[i].r * scale << "\t" << 0.0 << endl;
}

std::ostream& operator<<(std::ostream& os, Box* box)
{
	os << box->N << endl;

	for (int i = 0; i < box->N; i++)
	{
		os << box->particle[i].r << "\t" << box->particle[i].v << endl;
	}
}

std::istream& operator>>(std::istream& is, Box* box)
{
	is >> box->N;
	for (int i = 0; i < box->N; i++)
	{
		is >> box->particle[i].r;
		is >> box->particle[i].v;
	}
}

#endif
