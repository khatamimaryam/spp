#ifndef _FIELD_CELL_
#define _FIELD_CELL_

#include "c2dvector.h"
#include "parameters.h"
#include <vector>

class Field_Cell{
public:
	vector<Particle*> particle;

	C2DVector r,v;
	Real density;
	Real phi; // phi shows the amount of velocity cohesion
	Real omega;
	Real curl;

	void Init(Real,Real);
	void Delete();
	void Add(Particle* p);
	void Compute_Fields();
};


void Field_Cell::Init(Real x, Real y)
{
	r.x = x;
	r.y = y;
	Delete();
}

void Field_Cell::Delete()
{
	particle.clear();
}

void Field_Cell::Add(Particle* p)
{
	particle.push_back(p);
}

void Field_Cell::Compute_Fields()
{
	v.Null();
	phi = omega = 0;
	density = particle.size()*(grid_size*grid_size)/(L*L);
	for (int i = 0; i < particle.size(); i++)
	{
		v += particle[i]->v;
		omega += (particle[i]->r - r).x*particle[i]->v.y - (particle[i]->r - r).y*particle[i]->v.x;
		for (int j = i+1; j < particle.size(); j++)
			phi += (particle[i]->v*particle[j]->v)/sqrt(particle[i]->v.Square()*particle[j]->v.Square());
	}
	v /= particle.size();
	omega /= particle.size();
	phi /= particle.size()*(particle.size() - 1);
	phi *= 2;
	if (particle.size() < 2)
	{
		phi = 0;
		omega = 0;
		v.Null();
	}
}

#endif
